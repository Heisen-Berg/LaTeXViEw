package com.heisen_berg.andlatex;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.heisen_berg.andlatex.LatexTextView.LatexTextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    String sampleLatex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sampleLatex = "Let $$f(x)= \\frac {sin^{-1}(1-\\{x\\}).cos^{-1}(1-\\{x\\} )   }{\\sqrt{2\\{x\\}}.(1-\\{x\\})}$$ then find $\\mathrm{\\ Lim _{x\\to 0^+ }}$ and  $\\mathrm{\\ Lim_{x \\to 0^- }  }$ f(x),where { x }  denotes the fractional part function.";

        LatexTextView latexTextView = findViewById(R.id.latex_textview);
        latexTextView.setText(sampleLatex);

        Pattern p = Pattern.compile("(?i)\\$\\$(.*?)\\$\\$");
        Matcher matcher = p.matcher(sampleLatex);

        while (matcher.find()) {
            Log.i("vovo", "onCreate: " + matcher.group());
        }
    }

}